FROM alpine
ENV ASMUX_HOST=https://asmux.nova.chat
ENV FWD_HOST=127.0.0.1
ENV FWD_PORT=8000

RUN apk add curl jq bash openssh
WORKDIR /data
COPY . .
VOLUME /etc/ssh
CMD ["/data/run.sh"]
